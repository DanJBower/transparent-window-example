﻿namespace TransparentWindowExample.AccentInformation
{
    public enum AccentState
    {
        ACCENT_DISABLED = 0,
        ACCENT_ENABLE_GRADIENT = 1,
        ACCENT_ENABLE_TRANSPARENT_GRADIENT = 2,
        ACCENT_ENABLE_BLUR_BEHIND = 3,
        ACCENT_INVALID_STATE = 4
    }
}
